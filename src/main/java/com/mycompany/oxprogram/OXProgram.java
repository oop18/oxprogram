/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oxprogram;

import java.util.Scanner;

/**
 *
 * @author admin
 */
public class OXProgram {

    static char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'O';
    static int row, col;
    static Scanner sc = new Scanner(System.in);
    static boolean finish = false;

    public static void main(String[] args) {
        showWelcome();
        while (true) {
            showTable();
            showTurn();
            inputRowCol();
            process();
            if (finish) {
                break;
            }
        }

    }

    public static void showTable() {
        for (int r = 0; r < table.length; r++) {
            for (int c = 0; c < table[r].length; c++) {
                System.out.print(table[r][c]);
            }
            System.out.println("");
        }
    }

    public static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public static void showTurn() {
        System.out.println("Turn " + currentPlayer);
    }

    public static void inputRowCol() {
        System.out.println("Please input row,Col");
        row = sc.nextInt();
        col = sc.nextInt();
    }

    public static void process() {
        if (setTable()) {
            if (checkWin(table, currentPlayer, row, col)) {
                finish = true;
                showWin();
                return;
            }
            switchPlayer();
        }

    }

    private static void showWin() {
        showTable();
        if (checkDraw(table)) {
            System.out.println(">>>Draw<<<");
            return;
        } else {
            System.out.println(">>>" + currentPlayer + " Win<<<");
        }

    }

    public static void switchPlayer() {
        if (currentPlayer == 'O') {
            currentPlayer = 'X';
        } else {
            currentPlayer = 'O';
        }
    }

    public static boolean setTable() {
        table[row - 1][col - 1] = currentPlayer;
        return true;
    }

    private static boolean checkWin(char[][] table, char currenPlayer, int row, int col) {
        if (checkVertical(table, currenPlayer, col)) {
            return true;
        } else if (checkHorizontal(table, currenPlayer, row)) {
            return true;
        } else if (checkX(table, currenPlayer)) {
            return true;
        } else if (checkDraw(table)) {
            return true;
        }
        return false;

    }

    public static boolean checkVertical(char[][] table, char currenPlayer, int col) {
        for (int row = 0; row < table.length; row++) {
            if (table[row][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkHorizontal(char[][] table, char currenPlayer, int row) {
        for (int col = 0; col < table.length; col++) {
            if (table[row - 1][col] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkX(char[][] table, char currenPlayer) {
        if (checkX1(table, currenPlayer)) {
            return true;
        } else if (checkX2(table, currenPlayer)) {
            return true;
        }
        return false;

    }

    private static boolean checkX1(char[][] table, char currenPlayer) {
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX2(char[][] table, char currenPlayer) {
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkDraw(char[][] table) {
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table.length; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

}
