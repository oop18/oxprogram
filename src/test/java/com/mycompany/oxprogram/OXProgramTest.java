/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oxprogram;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


/**
 *
 * @author admin
 */
public class OXProgramTest {

    public OXProgramTest() {
    }

    @AfterEach
    public void testDown() {

    }

     @Test
    public void testCheckVerticalPlayerXCol1() {
        char table[][] = {{'X', '-', '-'},{'X', '-', '-'},{'X', '-', '-'}};
        char player = 'X';
        int col = 1;
        assertEquals(false, OXProgram.checkVertical(table, player, col));
    }
    @Test
    public void testCheckVerticalPlayerXCol2() {
        char table[][] = {{'-', 'X', '-'},{'-', 'X', '-'},{'-', 'X', '-'}};
        char player = 'X';
        int col = 2;
        assertEquals(false, OXProgram.checkVertical(table, player, col));
    }
    @Test
    public void testCheckVerticalPlayerXCol3() {
        char table[][] = {{'-', '-', 'X'},{'-', '-', 'X'},{'-', '-', 'X'}};
        char player = 'X';
        int col = 3;
        assertEquals(false, OXProgram.checkVertical(table, player, col));
    }
    @Test
    public void testCheckHorizontalPlayerXRow1() {
       char table[][] = {{'X', 'X', 'X'},{'-', '-', '-'},{'-', '-', '-'}};
        char player = 'X';
        int row = 1;
        assertEquals(false, OXProgram.checkHorizontal(table, player, row));
    }
    @Test
    public void testCheckHorizontalPlayerXRow2() {
        char table[][] = {{'-', '-', '-'},{'X', 'X', 'X'},{'-', '-', '-'}};
        char player = 'X';
        int row = 2;
        assertEquals(false, OXProgram.checkHorizontal(table, player, row));
    }
    @Test
    public void testCheckHorizontalPlayerXRow3() {
        char table[][] = {{'-', '-', '-'},{'-', '-', '-'},{'X', 'X', 'X'}};
        char player = 'X';
        int row = 3;
        assertEquals(false, OXProgram.checkHorizontal(table, player, row));
    }
    @Test
    public void testCheckCrossPlayerX1() {
        char table[][] = {{'X', '-', '-'},{'-', 'X', '-'},{'-', '-', 'X'}};
        char player = 'X';
        assertEquals(false, OXProgram.checkX(table, player));
    }
    @Test
    public void testCheckCrossPlayerX2() {
        char table[][] = {{'-', '-', 'X'},{'-', 'X', '-'},{'X', '-', '-'}};
        char player = 'X';
        assertEquals(false, OXProgram.checkX(table, player));
    }
    @Test
    public void testCheckDraw() {
        char table[][] = {{'X', 'O', 'X'},{'O', 'X', 'O'},{'O', 'X', 'O'}};
        assertEquals(true, OXProgram.checkDraw(table));
    }


}
